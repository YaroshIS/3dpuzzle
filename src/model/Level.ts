import Platform from "./platforms/Platform";
import {GlobalVariablesProvider} from "../providers/global-variables/global-variables";
import EmptyPlatform from "./platforms/Empty";
import StartPlatform from "./platforms/Start";
import SolidPlatform from "./platforms/Solid";
import SinglePlatform from "./platforms/Single";
import EndPlatform from "./platforms/End";

export class Level {
    private matrix: number[][];
    private field: Platform[][] = [];
    private platformSize: number;
    private startPlatform: Platform;

    constructor(matrixOfPlatformTypes: number[][], platformSize = GlobalVariablesProvider.CELL_SIZE) {
        this.matrix = matrixOfPlatformTypes;
        matrixOfPlatformTypes.forEach((row: number[], rowIndex: number) => {
            this.field[rowIndex] = [];
            row.forEach((platformType: number, platformIndex: number) => {
                switch (platformType) {
                    case GlobalVariablesProvider.PL_TYPES.EMPTY:
                        this.field[rowIndex].push(new EmptyPlatform(platformIndex, rowIndex));
                        break;
                    case GlobalVariablesProvider.PL_TYPES.START:
                        let platform;
                        if (!this.startPlatform){
                            platform = new StartPlatform(platformIndex, rowIndex);
                            this.startPlatform = platform;
                        } else {
                            platform = new SolidPlatform(platformIndex, rowIndex);
                        }
                        this.field[rowIndex].push(platform);
                        break;
                    case GlobalVariablesProvider.PL_TYPES.SOLID:
                        this.field[rowIndex].push(new SolidPlatform(platformIndex, rowIndex));
                        break;
                    case GlobalVariablesProvider.PL_TYPES.SINGLE:
                        this.field[rowIndex].push(new SinglePlatform(platformIndex, rowIndex));
                        break;
                    case GlobalVariablesProvider.PL_TYPES.END:
                        this.field[rowIndex].push(new EndPlatform(platformIndex, rowIndex));
                        break;
                    default:
                        this.field[rowIndex].push(new EmptyPlatform(platformIndex, rowIndex));
                        break;
                }

            })
        });
        this.platformSize = platformSize;
    }

    public getField(): Platform[][] {
        return this.field;
    }

    public getPlatformSize(): number {
        return this.platformSize;
    }

    public getStartPlatform(): Platform {
        return this.startPlatform;
    }

    public getMatrix(): number[][] {
        return this.matrix;
    }
}