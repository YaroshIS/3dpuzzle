export interface IPlatform{
    getType(): number,
    isSolid(): boolean
}