export interface IMesh{
    getGeometry(): THREE.Geometry,
    getMaterial(): THREE.Material,
    getMesh(): THREE.Mesh
}
