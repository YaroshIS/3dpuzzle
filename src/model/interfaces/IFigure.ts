export interface IFigure{
    moveUp(),
    moveLeft(),
    moveRight(),
    moveDown(),
    SetPosition(x:number, y:number)
}