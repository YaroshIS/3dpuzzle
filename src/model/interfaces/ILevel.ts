import Platform from "../platforms/Platform";
export interface ILevel{
    getMatrix(): number[][],
    getField(): Platform[][],
    getPlatformSize(): number,
    getStartPlatform(): Platform
}
