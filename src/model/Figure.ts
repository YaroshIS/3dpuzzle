import {IFigure} from "./interfaces/IFigure";
import {IMesh} from "./interfaces/IMesh";
import * as THREE from 'three';
import {GlobalVariablesProvider} from "../providers/global-variables/global-variables";
import {Events} from "ionic-angular";

export class Figure implements IFigure, IMesh {
    private geometry: THREE.Geometry;
    private material: THREE.Material;
    private mesh: THREE.Mesh;
    private events: Events;

    private infoBeforeMove: any;
    private infoAfterMove: any;

    constructor(x: number, z: number) {
        this.events = new Events();

        this.geometry = new THREE.BoxGeometry(GlobalVariablesProvider.CELL_SIZE, GlobalVariablesProvider.CELL_SIZE * 2, GlobalVariablesProvider.CELL_SIZE);
        this.material = new THREE.MeshLambertMaterial({color: GlobalVariablesProvider.FIGURE_COLOR});
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.matrixWorldNeedsUpdate = true;
        this.mesh.position.set(x, GlobalVariablesProvider.CELL_SIZE, z);
        this.infoAboutPosition.correspondingAxis = 'y';
        this.infoAboutPosition.position[0] = {x: x, z: z};
        this.mesh.localToWorld(new THREE.Vector3(0,0,0));
    }

    private infoAboutPosition = {
        correspondingAxis: 'y',
        position: [{x: 0, z: 0}]
    };

    public moveLeft() {
        this.infoBeforeMove = {
            meshPosition: {
                x: this.mesh.position.x,
                y: this.mesh.position.y,
                z: this.mesh.position.z
            },
            platformsPosition: this.infoAboutPosition.position
        };
        let cellSize = GlobalVariablesProvider.CELL_SIZE;
        switch (this.infoAboutPosition.correspondingAxis) {
            case 'y':
                this.rotateZ(Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'x';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x - cellSize, z: this.infoAboutPosition.position[0].z},
                    {x: this.infoAboutPosition.position[0].x - 2 * cellSize, z: this.infoAboutPosition.position[0].z}
                ];
                break;
            case 'x':
                this.rotateZ(Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'y';
                let indexOfCubeWithMinXCoord = this.infoAboutPosition.position[0].x < this.infoAboutPosition.position[1].x ? 0 : 1;
                this.infoAboutPosition.position = [
                    {
                        x: this.infoAboutPosition.position[indexOfCubeWithMinXCoord].x - cellSize,
                        z: this.infoAboutPosition.position[indexOfCubeWithMinXCoord].z
                    }
                ];
                break;
            case 'z':
                this.rotateZ(Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'z';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x - cellSize, z: this.infoAboutPosition.position[0].z},
                    {x: this.infoAboutPosition.position[1].x - cellSize, z: this.infoAboutPosition.position[1].z},

                ];
                break;
        }
    };

    public moveUp() {
        this.infoBeforeMove = {
            meshPosition: {
                x: this.mesh.position.x,
                y: this.mesh.position.y,
                z: this.mesh.position.z
            },
            platformsPosition: this.infoAboutPosition.position
        };
        let cellSize = GlobalVariablesProvider.CELL_SIZE;
        switch (this.infoAboutPosition.correspondingAxis) {
            case 'y':
                this.rotateX(-Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'z';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x, z: this.infoAboutPosition.position[0].z - cellSize},
                    {x: this.infoAboutPosition.position[0].x, z: this.infoAboutPosition.position[0].z - 2 * cellSize},
                ];
                break;
            case 'x':
                this.rotateX(-Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'x';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x, z: this.infoAboutPosition.position[0].z - cellSize},
                    {x: this.infoAboutPosition.position[1].x, z: this.infoAboutPosition.position[1].z - cellSize},
                ];
                break;
            case 'z':
                this.rotateX(-Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'y';
                let indexOfCubeWithMinZCoord = this.infoAboutPosition.position[0].z < this.infoAboutPosition.position[1].z ? 0 : 1;
                this.infoAboutPosition.position = [
                    {
                        x: this.infoAboutPosition.position[indexOfCubeWithMinZCoord].x,
                        z: this.infoAboutPosition.position[indexOfCubeWithMinZCoord].z - cellSize
                    },
                ];
                break;
        }
    };

    public moveRight() {
        this.infoBeforeMove = {
            meshPosition: {
                x: this.mesh.position.x,
                y: this.mesh.position.y,
                z: this.mesh.position.z
            },
            platformsPosition: this.infoAboutPosition.position
        };
        let cellSize = GlobalVariablesProvider.CELL_SIZE;
        switch (this.infoAboutPosition.correspondingAxis) {
            case 'y':
                this.rotateZ(-Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'x';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x + cellSize, z: this.infoAboutPosition.position[0].z},
                    {x: this.infoAboutPosition.position[0].x + 2 * cellSize, z: this.infoAboutPosition.position[0].z}
                ];
                break;
            case 'x':
                this.rotateZ(-Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'y';
                let indexOfCubeWithMaxXCoord = this.infoAboutPosition.position[0].x > this.infoAboutPosition.position[1].x ? 0 : 1;
                this.infoAboutPosition.position = [
                    {
                        x: this.infoAboutPosition.position[indexOfCubeWithMaxXCoord].x + cellSize,
                        z: this.infoAboutPosition.position[indexOfCubeWithMaxXCoord].z
                    }
                ];
                break;
            case 'z':
                this.rotateZ(-Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'z';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x + cellSize, z: this.infoAboutPosition.position[0].z},
                    {x: this.infoAboutPosition.position[1].x + cellSize, z: this.infoAboutPosition.position[1].z},

                ];
                break;
        }
    };

    public moveDown() {
        this.infoBeforeMove = {
            meshPosition: {
                x: this.mesh.position.x,
                y: this.mesh.position.y,
                z: this.mesh.position.z
            },
            platformsPosition: this.infoAboutPosition.position
        };
        let cellSize = GlobalVariablesProvider.CELL_SIZE;
        switch (this.infoAboutPosition.correspondingAxis) {
            case 'y':
                this.rotateX(Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'z';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x, z: this.infoAboutPosition.position[0].z + cellSize},
                    {x: this.infoAboutPosition.position[0].x, z: this.infoAboutPosition.position[0].z + 2 * cellSize},
                ];
                break;
            case 'x':
                this.rotateX(Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'x';
                this.infoAboutPosition.position = [
                    {x: this.infoAboutPosition.position[0].x, z: this.infoAboutPosition.position[0].z + cellSize},
                    {x: this.infoAboutPosition.position[1].x, z: this.infoAboutPosition.position[1].z + cellSize},
                ];
                break;
            case 'z':
                this.rotateX(Math.PI / 2);
                this.infoAboutPosition.correspondingAxis = 'y';
                let indexOfCubeWithMaxZCoord = this.infoAboutPosition.position[0].z > this.infoAboutPosition.position[1].z ? 0 : 1;
                this.infoAboutPosition.position = [
                    {
                        x: this.infoAboutPosition.position[indexOfCubeWithMaxZCoord].x,
                        z: this.infoAboutPosition.position[indexOfCubeWithMaxZCoord].z + cellSize
                    },
                ];
                break;
        }
    };

    public getMesh(): THREE.Mesh {
        return this.mesh;
    }

    public getGeometry(): THREE.Geometry {
        return this.geometry;
    }

    public getMaterial(): THREE.Material {
        return this.material;
    }

    public SetPosition(x: number, y: number) {
        this.mesh.position.x = x;
        this.mesh.position.z = y;
    }

    public getInfoAboutPosition() {
        return this.infoAboutPosition;
    }

    private rotateX(angle) {
        let xAxis = new THREE.Vector3(1, 0, 0);
        let pointOfRotate = new THREE.Vector3(0,0,this.mesh.position.z);
        let zCellDisplacement = this.infoAboutPosition.correspondingAxis == 'z' ? 1 : 0.5;
        pointOfRotate.setZ(angle < 0 ? this.mesh.position.z - (zCellDisplacement * GlobalVariablesProvider.CELL_SIZE) : this.mesh.position.z + (zCellDisplacement * GlobalVariablesProvider.CELL_SIZE));
        let yCellDisplacement = this.infoAboutPosition.correspondingAxis == 'y' ? 1 : 0.5;
        pointOfRotate.setY(this.mesh.position.y - (yCellDisplacement * GlobalVariablesProvider.CELL_SIZE));
        this.rotateAboutPoint(this.mesh, pointOfRotate, xAxis, angle, true);
    }

    private rotateZ(angle) {
        let zAxis = new THREE.Vector3(0, 0, 1);
        let pointOfRotate = new THREE.Vector3(this.mesh.position.x,0,0);
        let xCellDisplacement = this.infoAboutPosition.correspondingAxis == 'x' ? 1: 0.5;
        pointOfRotate.setX(angle < 0 ? this.mesh.position.x + (xCellDisplacement * GlobalVariablesProvider.CELL_SIZE) : this.mesh.position.x - (xCellDisplacement * GlobalVariablesProvider.CELL_SIZE));
        let yCellDisplacement = this.infoAboutPosition.correspondingAxis == 'y' ? 1: 0.5;
        pointOfRotate.setY(this.mesh.position.y - (yCellDisplacement * GlobalVariablesProvider.CELL_SIZE));
        this.rotateAboutPoint(this.mesh, pointOfRotate, zAxis, angle, true);
    }

    private rotateAroundWorldAxis(object: THREE.Object3D, axis, radians) {
        let rotWorldMatrix = new THREE.Matrix4();
        rotWorldMatrix.makeRotationAxis(axis.normalize(), radians);
        rotWorldMatrix.multiply(object.matrix);                // pre-multiply

        object.matrix = rotWorldMatrix;

        object.setRotationFromMatrix(object.matrix);
    }

    // obj - your object (THREE.Object3D or derived)
// point - the point of rotation (THREE.Vector3)
// axis - the axis of rotation (normalized THREE.Vector3)
// theta - radian value of rotation
// pointIsWorld - boolean indicating the point is in world coordinates (default = false)
    rotateAboutPoint(obj, point, axis, theta, pointIsWorld, currentAngle = 0) {
        let anglePerFrame = theta / GlobalVariablesProvider.FIGURE_FRAMES_PER_MOVE;
        if(Math.abs(currentAngle) < Math.abs(theta)) {
            setTimeout(() => {
                pointIsWorld = (pointIsWorld === undefined) ? false : pointIsWorld;

                if (pointIsWorld) {
                    obj.parent.localToWorld(obj.position); // compensate for world coordinate
                }

                obj.position.sub(point); // remove the offset
                obj.position.applyAxisAngle(axis, anglePerFrame); // rotate the POSITION
                obj.position.add(point); // re-add the offset

                if (pointIsWorld) {
                    obj.parent.worldToLocal(obj.position); // undo world coordinates compensation
                }

                // obj.rotateOnAxis(axis, theta); // rotate the OBJECT
                this.rotateAroundWorldAxis(obj, axis, anglePerFrame);

                currentAngle += anglePerFrame;

                this.rotateAboutPoint(obj,point,axis,theta,pointIsWorld,currentAngle);
            }, 70);
        } else {
            console.log('move end');
            this.infoAfterMove = {
                meshPosition: {
                    x: this.mesh.position.x,
                    y: this.mesh.position.y,
                    z: this.mesh.position.z
                },
                platformsPosition: this.infoAboutPosition.position
            };
            GlobalVariablesProvider.EVENTS.FIGURE_MOVE_COMPLETED.emit({
                before: this.infoBeforeMove,
                after: this.infoAfterMove
            });
        }
    }

    private GetFloorVertices() {
        let floorVertices = [];
        let floorYPosition = GlobalVariablesProvider.FLOOR_LEVEL;
        this.geometry.vertices.forEach((vertice) => {
            if (vertice.y == floorYPosition) {
                floorVertices.push(vertice);
            }
        });

        return floorVertices;
    }

    private GetCurrentHeight() {
        let floorLevel = GlobalVariablesProvider.FLOOR_LEVEL;
        let height;
        this.geometry.vertices.every((vertice) => {
            if (vertice.y != floorLevel) {
                height = Math.abs(vertice.y - floorLevel);
                return false;
            }
            return true;
        });

        return height;
    }

    private GetXWidth() {
        let firstXCoord = this.geometry.vertices[0].x;
        let secondXCoord;
        this.geometry.vertices.every((vertice) => {
            if (vertice.x != firstXCoord) {
                secondXCoord = vertice.x;
                return false;
            }
            return true;
        });

        return Math.abs(firstXCoord - secondXCoord);
    }

    private GetZWidth() {
        let firstZCoord = this.geometry.vertices[0].z;
        let secondZCoord;
        this.geometry.vertices.every((vertice) => {
            if (vertice.x != firstZCoord) {
                secondZCoord = vertice.z;
                return false;
            }
            return true;
        });

        return Math.abs(firstZCoord - secondZCoord);
    }
}
