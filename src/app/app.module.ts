import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from '@ionic/storage';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {SQLite} from "@ionic-native/sqlite";
import {ComponentsModule} from "../components/components.module";
import {GlobalVariablesProvider} from '../providers/global-variables/global-variables';
import {PlayPageModule} from "../pages/play/play.module";
import {IonicSwipeAllModule} from 'ionic-swipe-all';
import {LevelProvider} from '../providers/level/level';


@NgModule({
    declarations: [
        MyApp,
        HomePage,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        ComponentsModule,
        PlayPageModule,
        IonicSwipeAllModule,
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        SQLite,
        GlobalVariablesProvider,
        LevelProvider,
    ]
})
export class AppModule {
}
