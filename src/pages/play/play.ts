import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ScenegraphComponent} from "../../components/scenegraph/scenegraph";
import {ILevel} from "../../model/interfaces/ILevel";

import {Level} from "../../model/Level";
import {GlobalVariablesProvider} from "../../providers/global-variables/global-variables";
import {LevelProvider} from "../../providers/level/level";

/**
 * Generated class for the PlayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-play',
    templateUrl: 'play.html',
    providers: [GlobalVariablesProvider]
})
export class PlayPage {
    public deleteModeEnabled: boolean = false;
    public cubeColor: string = 'rgb(255,255,255)';
    private level;

    private subscriptions = {
        levelCompleted: GlobalVariablesProvider.EVENTS.LEVEL_COMPLETED.subscribe(() => {
            this.sceneGraph.RemoveLevel();
            this.levelProvider.getUserLevel().then((levelInfo) => {
                this.level = new Level(levelInfo.field);
                this.sceneGraph.LoadLevel(this.level);
                this.sceneGraph.StartLevel();
            })
        })
    };

    @ViewChild('scenegraph')
    sceneGraph: ScenegraphComponent;

    constructor(public navCtrl: NavController,
                private levelProvider: LevelProvider) {

    }

    ionViewDidEnter() {
        this.sceneGraph.startAnimation();
    }

    ionViewDidLeave() {
        this.sceneGraph.stopAnimation();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PlayPage');
    }

    ngAfterViewInit() {
        this.levelProvider.getUserLevel().then((levelInfo) => {
            this.level = new Level(levelInfo.field);
            this.sceneGraph.startAnimation();
            this.sceneGraph.LoadLevel(this.level);
            this.sceneGraph.StartLevel();
        });
    }
}
