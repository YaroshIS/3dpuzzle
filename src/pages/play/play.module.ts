import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PlayPage} from './play';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
    declarations: [
        PlayPage,
    ],
    imports: [
        IonicPageModule.forChild(PlayPage),
        ComponentsModule,
    ],
})
export class PlayPageModule {
}
