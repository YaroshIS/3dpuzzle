import { NgModule } from '@angular/core';
import { ScenegraphComponent } from './scenegraph/scenegraph';
import {IonicSwipeAllModule} from "ionic-swipe-all";
@NgModule({
	declarations: [ScenegraphComponent],
	imports: [IonicSwipeAllModule],
	exports: [ScenegraphComponent]
})
export class ComponentsModule {}
