import {Component, ElementRef} from '@angular/core';
import * as THREE from 'three';
import {ILevel} from "../../model/interfaces/ILevel";
import {Figure} from "../../model/Figure";
import {GlobalVariablesProvider} from "../../providers/global-variables/global-variables";
import {ToastController} from "ionic-angular";

/**
 * Generated class for the ScenegraphComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'scenegraph',
    templateUrl: 'scenegraph.html',
    providers: [GlobalVariablesProvider]
})
export class ScenegraphComponent {
    private readonly DEFAULT_CUBESIZE: number = 50;

    private cubeGeo: THREE.BoxGeometry;
    private cubeMat: THREE.MeshLambertMaterial;
    private raycaster: THREE.Raycaster;
    private mouse: THREE.Vector2;
    private renderer: THREE.WebGLRenderer;
    private scene: THREE.Scene;
    private camera: THREE.Camera;
    private hemisphereLight: THREE.HemisphereLight;
    private animating: boolean;
    private objects = [];
    // Size of drawing area
    private width: number;
    private height: number;

    private level: ILevel;
    private figure: Figure;

    private allowToMove = true;
    private subscriptions = {
        figureMoved: GlobalVariablesProvider.EVENTS.FIGURE_MOVE_COMPLETED.subscribe((position) => {
            this.allowToMove = true;
            GlobalVariablesProvider.EVENTS.CHECK_POSITION.emit();
        }),
        checkPosition: GlobalVariablesProvider.EVENTS.CHECK_POSITION.subscribe(() => {
            this.CheckPosition();
        }),
        singlePlatformDisappeared: GlobalVariablesProvider.EVENTS.SINGLE_PLATFORM_DESTROYED.subscribe((platfromMesh: THREE.Object3D) => {
            this.CheckPosition();
        }),
        restartLevel: GlobalVariablesProvider.EVENTS.RESTART_LEVEL.subscribe(() => {
            this.RestartLevel();
        })
    };

    constructor(public sceneGraphElement: ElementRef,
                private toastCtrl: ToastController) {
    }

    ngAfterViewInit() {
        //Scene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xf0f0f0);

        //Camera
        let screenWidth = this.sceneGraphElement.nativeElement.childNodes[0].clientWidth;
        let screenHeight = this.sceneGraphElement.nativeElement.childNodes[0].clientHeight;
        this.camera = new THREE.OrthographicCamera(-screenWidth / 2, screenWidth / 2, screenHeight / 2, -screenHeight / 2, -1000, 1000);
        this.camera.position.set(60, 44, 60);
        this.camera.lookAt(new THREE.Vector3());

        //Cubes
        this.cubeGeo = new THREE.BoxGeometry(this.DEFAULT_CUBESIZE, this.DEFAULT_CUBESIZE, this.DEFAULT_CUBESIZE);
        this.cubeMat = new THREE.MeshLambertMaterial({color: 0xFFF5EE});

        //Helpers
        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();

        //Light
        this.hemisphereLight = new THREE.HemisphereLight(0xeeeeff, 0x777788, 1.3);
        this.scene.add(this.hemisphereLight);

        //Renderer
        this.renderer = new THREE.WebGLRenderer({alpha: true});
        this.renderer.shadowMap.enabled = true;
        this.sceneGraphElement.nativeElement.childNodes[0].appendChild(this.renderer.domElement);

        this.width = this.sceneGraphElement.nativeElement.childNodes[0].clientWidth;
        this.height = this.sceneGraphElement.nativeElement.childNodes[0].clientHeight;

        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
    }

    startAnimation() {
        let width = this.sceneGraphElement.nativeElement.childNodes[0].clientWidth;
        let height = this.sceneGraphElement.nativeElement.childNodes[0].clientHeight;
        this.renderer.setSize(width, height);
        this.animating = true;
        this.render();
    }

    stopAnimation() {
        this.animating = false;
    }

    render() {
        this.renderer.render(this.scene, this.camera);
        if (this.animating) {
            requestAnimationFrame(() => {
                this.render()
            });
        }
    }

    rgbToHex(r, g, b): string {
        return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }

    public LoadLevel(level: ILevel) {
        this.level = level;
        level.getField().forEach((row, rowIndex) => {
            row.forEach((platform, platformIndex) => {
                if (platform.getMesh() !== null) {
                    this.scene.add(platform.getMesh());
                }
            });
        })
    }

    private RedrawLevel() {
        this.level.getField().forEach((row, rowIndex) => {
            row.forEach((platform, platformIndex) => {
                if (platform.getMesh() !== null) {
                    this.scene.remove(platform.getMesh());
                }
            });
        });

        this.level.getField().forEach((row, rowIndex) => {
            row.forEach((platform, platformIndex) => {
                if (platform.getMesh() !== null) {
                    this.scene.add(platform.getMesh());
                }
            });
        })
    }

    private RestartLevel() {
        this.scene.remove(this.figure.getMesh());
        this.figure = null;

        let startPlatform = this.level.getStartPlatform().getMesh();
        this.figure = new Figure(startPlatform.position.x, startPlatform.position.z);
        let figureMesh = this.figure.getMesh();
        this.scene.add(figureMesh);
        this.camera.position.set(figureMesh.position.x, figureMesh.position.y, figureMesh.position.z);

        this.allowToMove = true;
    }

    public StartLevel(): boolean {
        if (this.level) {
            this.RedrawLevel();
            let startPlatform = this.level.getStartPlatform().getMesh();
            this.figure = new Figure(startPlatform.position.x, startPlatform.position.z);
            let figurePosition = this.figure.getMesh().position;
            this.scene.add(this.figure.getMesh());
            this.camera.position.set(figurePosition.x + 40, figurePosition.y + 80, figurePosition.z + 80);
            this.camera.lookAt(figurePosition);
            this.allowToMove = true;
            return true;
        } else {
            return false
        }
    }

    private SwipeUpEvent() {
        if (this.allowToMove) {
            this.allowToMove = false;
            this.figure.moveUp();

            let infoAboutFigurePosition = this.figure.getInfoAboutPosition();

            let cameraXDisplacement = 0;
            let cameraZDisplacement = 0;

            cameraZDisplacement = (infoAboutFigurePosition.correspondingAxis === 'y' || infoAboutFigurePosition.correspondingAxis === 'z') ?
            -GlobalVariablesProvider.CELL_SIZE * 1.25 :
                -GlobalVariablesProvider.CELL_SIZE;

            this.MoveCamera(cameraXDisplacement, cameraZDisplacement);
        }
    }

    private SwipeDownEvent() {
        if (this.allowToMove) {
            this.allowToMove = false;
            this.figure.moveDown();

            let infoAboutFigurePosition = this.figure.getInfoAboutPosition();

            let cameraXDisplacement = 0;
            let cameraZDisplacement = 0;

            cameraZDisplacement = (infoAboutFigurePosition.correspondingAxis === 'y' || infoAboutFigurePosition.correspondingAxis === 'z') ?
            GlobalVariablesProvider.CELL_SIZE * 1.25 :
                GlobalVariablesProvider.CELL_SIZE;

            this.MoveCamera(cameraXDisplacement, cameraZDisplacement);
        }
    }

    private SwipeLeftEvent() {
        if (this.allowToMove) {
            this.allowToMove = false;
            this.figure.moveLeft();

            let infoAboutFigurePosition = this.figure.getInfoAboutPosition();

            let cameraXDisplacement = 0;
            let cameraZDisplacement = 0;

            cameraXDisplacement = (infoAboutFigurePosition.correspondingAxis === 'y' || infoAboutFigurePosition.correspondingAxis === 'x') ?
            -GlobalVariablesProvider.CELL_SIZE * 1.25 :
                -GlobalVariablesProvider.CELL_SIZE;

            this.MoveCamera(cameraXDisplacement, cameraZDisplacement);
        }
    }

    private SwipeRightEvent() {
        if (this.allowToMove) {
            this.allowToMove = false;
            this.figure.moveRight();

            let infoAboutFigurePosition = this.figure.getInfoAboutPosition();
            let cameraXDisplacement = 0;
            let cameraZDisplacement = 0;

            cameraXDisplacement = (infoAboutFigurePosition.correspondingAxis === 'y' || infoAboutFigurePosition.correspondingAxis === 'x') ?
            GlobalVariablesProvider.CELL_SIZE * 1.25 :
                GlobalVariablesProvider.CELL_SIZE;

            this.MoveCamera(cameraXDisplacement, cameraZDisplacement);
        }
    }

    private GetFigureStatus() {
        let currentFigurePosition = this.figure.getInfoAboutPosition().position;
        let matrixOfPlatforms = this.level.getField();

        let isLose: boolean = false;
        let isWon: boolean = false;

        let result = GlobalVariablesProvider.FIGURE_STATUSES.STAND_ON_PLATFORMS;

        for (let index = 0; index < currentFigurePosition.length; index++) {
            let position = currentFigurePosition[index];

            let i = position.x / this.level.getPlatformSize();
            let j = position.z / this.level.getPlatformSize();

            if (!(matrixOfPlatforms[j]) || !(matrixOfPlatforms[j][i]) || matrixOfPlatforms[j][i].isSolid() === false) {
                result = GlobalVariablesProvider.FIGURE_STATUSES.FALL;
                break;
            }

            if (matrixOfPlatforms[j][i].getType() === GlobalVariablesProvider.PL_TYPES.END && this.figure.getInfoAboutPosition().correspondingAxis === 'y') {
                result = GlobalVariablesProvider.FIGURE_STATUSES.STAND_ON_WIN_PLATFORM;
                break;
            }
        }

        return result;
    }

    private CheckPosition() {
        let figureStatus = this.GetFigureStatus();

        switch (figureStatus) {
            case GlobalVariablesProvider.FIGURE_STATUSES.STAND_ON_PLATFORMS:
                break;
            case GlobalVariablesProvider.FIGURE_STATUSES.STAND_ON_WIN_PLATFORM:
                if (this.allowToMove) {
                    this.allowToMove = false;

                    let messageWon = this.toastCtrl.create({
                        message: 'You won!',
                        duration: 1000,
                        position: 'middle',
                        cssClass: 'toast-message'
                    });
                    messageWon.present();
                    GlobalVariablesProvider.EVENTS.FIGURE_STAND_ON_WIN_PLATFORM.emit();
                }
                break;
            case GlobalVariablesProvider.FIGURE_STATUSES.FALL:
                if (this.allowToMove) {
                    this.allowToMove = false;

                    let messageLose = this.toastCtrl.create({
                        message: 'You lose!',
                        duration: 1000,
                        position: 'middle',
                        cssClass: 'toast-message'
                    });
                    messageLose.present();
                    setTimeout(() => {
                        GlobalVariablesProvider.EVENTS.RESTART_LEVEL.emit();
                    }, 1000);
                }
                break;
        }
    }

    public RemoveLevel(){
        this.scene.remove(this.figure.getMesh());
        this.level.getField().forEach((row, rowIndex) => {
            row.forEach((platform, platformIndex) => {
                if (platform.getMesh() !== null) {
                    this.scene.remove(platform.getMesh());
                }
            });
        });
    }

    private MoveCamera(xDisplacement, zDisplacement, currentXDisplacement = 0, currentZDisplacement = 0) {
        let xDisplPerFrame = xDisplacement / GlobalVariablesProvider.CAMERA_MOVE_PER_FRAME;
        let zDisplPerFrame = zDisplacement / GlobalVariablesProvider.CAMERA_MOVE_PER_FRAME;

        if (Math.abs(currentXDisplacement) < Math.abs(xDisplacement) || Math.abs(currentZDisplacement) < Math.abs(zDisplacement)) {
            setTimeout(() => {
                this.camera.position.add(new THREE.Vector3(xDisplPerFrame, 0, zDisplPerFrame));
                currentXDisplacement += xDisplPerFrame;
                currentZDisplacement += zDisplPerFrame;

                this.MoveCamera(xDisplacement, zDisplacement, currentXDisplacement, currentZDisplacement);
            }, 50)
        }
    }

    ngOnDestroy() {
        this.subscriptions.checkPosition.unsubscribe();
        this.subscriptions.figureMoved.unsubscribe();
        this.subscriptions.singlePlatformDisappeared.unsubscribe();
    }
}
