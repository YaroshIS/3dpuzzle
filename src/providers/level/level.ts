import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {GlobalVariablesProvider} from "../global-variables/global-variables";

/*
 Generated class for the LevelProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class LevelProvider {
    private levels = [
        {
            ID: 1,
            field: [
                [0, 3, 3, 3, 0],
                [1, 3, 3, 3, 2],
                [0, 3, 3, 3, 0],
            ]
        },
        {
            ID: 2,
            field: [
                [1, 3, 0, 3, 2],
                [3, 3, 0, 3, 3],
                [3, 3, 3, 3, 3],
                [0, 3, 3, 3, 0],
            ]
        },
        {
            ID: 3,
            field: [
                [1, 3, 3, 0, 0],
                [3, 3, 3, 0, 0],
                [3, 3, 3, 3, 3],
                [0, 0, 0, 3, 3],
                [0, 3, 3, 3, 3],
                [0, 3, 3, 3, 3],
                [0, 3, 0, 0, 0],
                [3, 3, 3, 3, 0],
                [3, 3, 2, 3, 0],
                [0, 3, 3, 3, 0],
            ]
        },
        {
            ID: 4,
            field: [
                [1, 0, 0, 0, 0, 0, 0],
                [3, 4, 4, 4, 4, 4, 3],
                [3, 4, 4, 4, 4, 4, 3],
                [0, 0, 0, 3, 0, 0, 3],
                [0, 0, 0, 2, 4, 4, 3],
                [0, 0, 0, 0, 4, 4, 3],
                [0, 0, 0, 0, 4, 4, 3],
            ]
        },
        {
            ID: 5,
            field: [
                [0,0,0,0,0,3,3,3,0,0],
                [3,3,3,3,0,3,3,3,0,0],
                [3,0,0,3,3,3,0,3,3,3],
                [3,3,3,3,1,3,2,3,3,3],
                [3,3,3,0,3,3,0,3,0,0],
                [0,0,0,0,0,3,0,3,0,0],
                [0,0,0,0,0,3,3,3,0,0],
                [0,0,0,0,0,3,3,3,0,0],
            ]
        },
        {
            ID: 6,
            field: [
                [4,4,0,4,3,4],
                [4,4,1,4,4,0],
                [2,4,4,4,4,3],
                [4,4,4,4,4,4],
                [4,4,0,4,4,3],
                [0,3,4,3,4,4],
                [4,3,4,4,4,4],
                [3,4,4,4,3,3],
            ]
        }
    ];

    private subscriptions = {
        levelCompleted: GlobalVariablesProvider.EVENTS.FIGURE_STAND_ON_WIN_PLATFORM.subscribe(() => {
            this.storage.get('countOfCompletedLevels').then((count) => {
                this.storage.set('countOfCompletedLevels', ++count).then(() => {
                    GlobalVariablesProvider.EVENTS.LEVEL_COMPLETED.emit();
                });
            })
        })
    };

    constructor(private storage: Storage) {

        this.storage.get('countOfCompletedLevels').then((count) => {
            if(!count){
                this.storage.set('countOfCompletedLevels', '0');
            }
        });
        console.log('Hello LevelProvider Provider');
    }

    public getUserLevel(): Promise<{ID: number, field: number[][]}>{
        return new Promise((resolve) => {
            this.storage.get('countOfCompletedLevels').then((count) => {
                if(this.levels[+count]) {
                    resolve(this.levels[+count]);
                } else {
                    this.storage.set('countOfCompletedLevels', '0').then(() => {
                        resolve(this.levels[0]);
                    })
                }
            });
        })
    }



}
