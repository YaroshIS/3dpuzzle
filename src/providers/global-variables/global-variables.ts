import {EventEmitter, Injectable} from '@angular/core';
/*
 Generated class for the GlobalVariablesProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class GlobalVariablesProvider {
    // PL - platform(s)

    public static readonly FIGURE_COLOR = 0x808000;
    public static readonly CELL_SIZE = 50;
    public static readonly PL_HEIGHT = 10;
    public static readonly FLOOR_LEVEL = -50;
    public static readonly FIGURE_FRAMES_PER_MOVE = 5;
    public static readonly CAMERA_MOVE_PER_FRAME = 7;
    public static readonly TIME_OF_DESTROYING_SINGE_PL = 1000;
    public static readonly WINDOW_WIDTH = 500;
    public static readonly WINDOW_HEIGHT = 300;

    public static readonly EVENTS = {
        CHECK_POSITION: new EventEmitter(),
        FIGURE_MOVE_COMPLETED: new EventEmitter(),
        SINGLE_PLATFORM_DESTROYED: new EventEmitter(),
        RESTART_LEVEL: new EventEmitter(),
        LEVEL_COMPLETED: new EventEmitter(),
        FIGURE_STAND_ON_WIN_PLATFORM: new EventEmitter()
    };

    public static readonly FIGURE_STATUSES = {
        STAND_ON_PLATFORMS: 1,
        FALL: 2,
        STAND_ON_WIN_PLATFORM: 3

    };

    public static readonly PL_COLORS = {
        EMPTY: 0xffffff,
        END: 0x00e600,
        SOLID: 0x808080,
        START: 0xedd977,
        SINGLE: 0x9c6308,
        SINGLE_DESTROYING: 0xdc143c
    };

    public static readonly PL_TYPES = {
        EMPTY: 0,
        START: 1,
        END: 2,
        SOLID: 3,
        SINGLE: 4,
    };

    constructor() {
        console.log('Hello GlobalVariablesProvider Provider');
    }

}
